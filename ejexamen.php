<?php

// Definir la constante para el tamaño de la matriz
define("TAMANO_MATRIZ", 3); // Puedes cambiar este valor según tus necesidades

// Función para generar una matriz cuadrada con números aleatorios
function generarMatriz($tamano) {
    $matriz = array();
    for ($i = 0; $i < $tamano; $i++) {
        $fila = array();
        for ($j = 0; $j < $tamano; $j++) {
            $fila[] = rand(0, 9);
        }
        $matriz[] = $fila;
    }
    return $matriz;
}

// Función para imprimir la matriz y calcular la suma de la diagonal principal
function imprimirMatrizYCalcularDiagonal($matriz) {
    $sumaDiagonal = 0;
    echo "<table border='1'>";
    for ($i = 0; $i < count($matriz); $i++) {
        echo "<tr>";
        for ($j = 0; $j < count($matriz[$i]); $j++) {
            echo "<td>" . $matriz[$i][$j] . "</td>";
            if ($i == $j) {
                $sumaDiagonal += $matriz[$i][$j];
            }
        }
        echo "</tr>";
    }
    echo "</table>";
    echo "La suma de la diagonal principal es: $sumaDiagonal<br>";
    return $sumaDiagonal;
}

// Ciclo para generar matrices hasta que la suma de la diagonal principal esté entre 10 y 15
do {
    $matriz = generarMatriz(TAMANO_MATRIZ);
    $sumaDiagonal = imprimirMatrizYCalcularDiagonal($matriz);
} while ($sumaDiagonal < 10 || $sumaDiagonal > 15);

echo "La suma de la diagonal principal está entre 10 y 15. Finalización del script.";

?>

